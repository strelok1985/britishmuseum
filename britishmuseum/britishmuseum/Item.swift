//
//  Menu.swift
//  britishmuseum
//
//  Created by Andrey Bakanov on 4/14/19.
//  Copyright © 2019 Andrey Bakanov. All rights reserved.
//

import Foundation
import Parse

class Item : PFObject, PFSubclassing {
   @NSManaged var name: String?
   @NSManaged var dist: String?
   @NSManaged var image: String?
   @NSManaged var info: String?
   @NSManaged var xPos: String?
   @NSManaged var zPos: String?
   @NSManaged var yPos: String?

    class func parseClassName() -> String {
        return "Items"
    }
}
