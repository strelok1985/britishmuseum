//
//  itemInfoTest.swift
//  britishmuseum
//
//  Created by Andrey Bakanov on 4/15/19.
//  Copyright © 2019 Andrey Bakanov. All rights reserved.
//

import UIKit
import Parse
import AVFoundation

class itemInfoTest: UIViewController {
    
    var itemToDisplay: Item?
    var mafon: AVAudioPlayer!
    
    @IBOutlet weak var imageItem: UIImageView!{
        didSet{
            guard let image = itemToDisplay?.image else {return}
            imageItem.image = UIImage(named: image)
        }
    }
    
    @IBOutlet weak var itemName: UILabel!{
        didSet{
            itemName.text = itemToDisplay?.name
        }
    }
    
    @IBOutlet weak var itemDest: UILabel!{
        didSet{
            itemDest.text = itemToDisplay?.dist
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "itemToMap"{
            if let vc = segue.destination as? Map {
                let menu = sender as? Item
                print(menu ?? "no info")
                vc.itemToDisplay = menu
                
                
            }
        }
        
    }
    
    @IBOutlet weak var itemText: UITextView!{
        didSet{
            itemText.text = itemToDisplay?.info
        }
    }
    
    @IBOutlet weak var playSound: UISwitch!
    
    @IBAction func showOnMap(_ sender: Any) {
        
        self.performSegue(withIdentifier: "itemToMap", sender: itemToDisplay)
        
    }
    
   
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.playSound.setOn(false, animated: true)
        self.playSound.addTarget(self, action: #selector(playVoice(paramTarget:)), for: .valueChanged)
        

        // Do any additional setup after loading the view.
    }
    
    @objc func playVoice(paramTarget: UISwitch) {
        guard let filePath = Bundle.main.url(forResource: itemToDisplay?.image, withExtension: "mp3")  else {
            return
        }
        mafon = try? AVAudioPlayer(contentsOf: filePath)
        if paramTarget.isOn{
        mafon.play()}
        else {mafon.stop()}


    }
    
}
