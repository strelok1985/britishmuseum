//
//  MenuCollectionViewCell.swift
//  britishmuseum
//
//  Created by Andrey Bakanov on 4/14/19.
//  Copyright © 2019 Andrey Bakanov. All rights reserved.
//

import UIKit
import Parse

class MenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageItem: UIImageView!
    
    @IBOutlet weak var itemName: UILabel!
    
    @IBOutlet weak var itemDest: UILabel!
   
    var menu: Item? {
        didSet {
            itemName.text = menu?.name
            itemDest.text = menu?.dist
            if let image = menu?.image{
                imageItem.image = UIImage(named: image)
            }
        }
    }

}
