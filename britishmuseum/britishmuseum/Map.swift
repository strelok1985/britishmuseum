//
//  GameViewController.swift
//  test3d
//
//  Created by Andrey Bakanov on 4/3/19.
//  Copyright © 2019 Andrey Bakanov. All rights reserved.
//

import UIKit
import QuartzCore
import SceneKit
//let museumModel :String = String()
var scene = SCNScene()
var xPoint: Float = 10
//10
var yPoint: Float  = 90
var zPoint: Float = -5
//-5



class Map: UIViewController, SCNSceneRendererDelegate {
    
    var scnView = SCNView()
    
    @IBOutlet var containerView: UIView!
    var itemToDisplay: Item?
    
    var camera = SCNCamera()
    var cameraNode1 = SCNNode()
    var museumModel1 = SCNNode()
    var nameNode = SCNNode()
    open var worldNode = SCNNode()
    
    var centreNode = SCNNode()
    var characterScene = SCNScene(named: "3dmap1.scn")!
    
    static var layersOverlay: SKOverlay?
    
       
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // create a new scene
        scene = SCNScene(named: "3dmap1.scn")!
        
        
        centreNode.position = SCNVector3(x: 0, y: 0, z: 0)
        
        // create and add a camera to the scene
        let cameraNode = cameraNode1
        cameraNode.camera = camera
        scene.rootNode.addChildNode(cameraNode)
        
        
        Map.layersOverlay = SKOverlay(main: self, size: self.view.frame.size)
        
        // place the camera
        xPoint = Float(itemToDisplay?.xPos ?? "10") as! Float
        yPoint = Float(itemToDisplay?.yPos ?? "90") as! Float
        zPoint = Float(itemToDisplay?.zPos ?? "-5") as! Float
        cameraNode.position = SCNVector3(x: xPoint, y: yPoint, z: zPoint)
        cameraNode.rotation = SCNVector4(-1, 0, 0, 90 * Float.pi / 180) //persp
        
       
        addLight()
        ligt()
        setupView()
        addContainerButtons()
        
        
//
        
        
        // retrieve the ship node
         museumModel1 = scene.rootNode.childNode(withName: "map", recursively: true)!
       // scene.rootNode.isHidden = false
        print(itemToDisplay?.xPos ?? "no info")
        
        
        
        // animate the 3d object
       // museumModel.runAction(SCNAction.repeatForever(SCNAction.rotateBy(x: 0, y: 2, z: 0, duration: 1)))
        
        // retrieve the SCNView
        
        // add a tap gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        scnView.addGestureRecognizer(tapGesture)
        
    }
    
    func setupView(){
        scnView = self.view as! SCNView
        
        // set the scene to the view
        scnView.scene = scene
        //        scnView.delegate = scene as! SCNSceneRendererDelegate
        scnView.delegate = self
        
        scnView.overlaySKScene = Map.layersOverlay
        
        
        // allows the user to manipulate the camera
        scnView.allowsCameraControl = true
        
        // show statistics such as fps and timing information
        scnView.showsStatistics = false
        
        // configure the view
        scnView.backgroundColor = UIColor.white
        
        
        
        
        
        
    }
    
    
    func addLight(){
        // create and add a light to the scene
        let lightNode = SCNNode()
        lightNode.light = SCNLight()
        lightNode.light!.type = .omni
        lightNode.position = SCNVector3(x: 0, y: 10, z: 40)
        scene.rootNode.addChildNode(lightNode)
        
        // create and add an ambient light to the scene
        let ambientLightNode = SCNNode()
        ambientLightNode.light = SCNLight()
        ambientLightNode.light!.type = .ambient
        ambientLightNode.light!.color = UIColor.darkGray
        scene.rootNode.addChildNode(ambientLightNode)
    }
    
    
    func ligt() {
        if itemToDisplay?.image! != nil{
            let nameNode1 = String(itemToDisplay?.image! ?? "museumModel")
            print("from base \(nameNode1)")
            
            nameNode = scene.rootNode.childNode(withName: "\(nameNode1)" + "-", recursively: true)!
            print("from scnassets  \(nameNode)")
            let material = nameNode.geometry!.firstMaterial!
            
            // highlight it
            SCNTransaction.begin()
            SCNTransaction.animationDuration = 1
            
            // on completion - unhighlight
            SCNTransaction.completionBlock = {
                SCNTransaction.begin()
                SCNTransaction.animationDuration = 1
                
                material.emission.contents = UIColor.blue
                
                SCNTransaction.commit()
            }
            
            material.emission.contents = UIColor.red
            
            SCNTransaction.commit()
        }
    }
    func addContainerButtons(){
        //add contaiber for buttons
        self.view.addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        let tabBarH = self.tabBarController?.tabBar.frame.size.height
        NSLayoutConstraint.activate([
            containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            containerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -tabBarH!),
            ])
    }
    
    func renderer(_ renderer: SCNSceneRenderer,
                  updateAtTime time: TimeInterval){
        //print("orientation camera \(scene.rootNode.orientation)")
       // print("orientation camera \(cameraNode1.eulerAngles)")
        //let cameraNode2 = scnView.pointOfView
        //print("orientation camera \(camera.po)")
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mapToItem"{
            if let vc = segue.destination as? itemInfoTest {
                let menu = sender as? Item
                print(menu ?? "no info")
                vc.itemToDisplay = menu
                
            }
        }
        
    }
    
    
    @objc
    func handleTap(_ gestureRecognize: UIGestureRecognizer) {
        // retrieve the SCNView
        let scnView = self.view as! SCNView
        
        
        // check what nodes are tapped
        let p = gestureRecognize.location(in: scnView)
        let hitResults = scnView.hitTest(p, options: [:])
        
        
        // check that we clicked on at least one object
        if hitResults.count > 0 {
             let result = hitResults[0]
            // retrieved the first clicked object
           
            let itemName = result.node.name!
            let endSent = itemName.firstIndex(of: "-")!
            var nodeName = itemName [...endSent]
                nodeName.remove(at: nodeName.index(before: nodeName.endIndex))
            print("noda name posle obrabptki \(nodeName)")
            
           
            let neededItem = itemArray.filter{ $0.image! == nodeName }.first
            if neededItem != nil{
                self.performSegue(withIdentifier: "mapToItem", sender: neededItem)}
           
            
                    let material = result.node.geometry!.firstMaterial!
                    
                    // highlight it
                    SCNTransaction.begin()
                    SCNTransaction.animationDuration = 0.5
                    
                    // on completion - unhighlight
                    SCNTransaction.completionBlock = {
                        SCNTransaction.begin()
                        SCNTransaction.animationDuration = 0.1
                        
                        material.emission.contents = UIColor.blue
                        
                        SCNTransaction.commit()
                    }
                    
                    material.emission.contents = UIColor.red
                    
                    SCNTransaction.commit()
                    
                }
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    
    func checkNodeAtPosition(_ touch: UITouch) -> String?{
        if let skOverlay = Map.layersOverlay{
            let location = touch.location(in: scnView)
            let node = skOverlay.atPoint(CGPoint(x: location.x, y: self.scnView.frame.size.height - location.y))
            if let name = node.name{
                return name
            } else {
                return nil
            }
            
        }else {
            return nil
        }
        
    }
    
    func touchesFunction(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first{
            if let name = checkNodeAtPosition(touch){
                if name == "1_flour" {
                    if museumModel1.isHidden == true {
                        museumModel1.isHidden = false
                        print("first done")
                    }
                    else if museumModel1.isHidden == false {
                        museumModel1.isHidden = true
                        print("second done")
                    }
                }
            }
        }
    }
    
 
        
    }
    
    
    
   



