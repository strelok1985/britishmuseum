//
//  Items.swift
//  britishmuseum
//
//  Created by Andrey Bakanov on 4/4/19.
//  Copyright © 2019 Andrey Bakanov. All rights reserved.
//

import UIKit


  
class ItemsVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self        
       
        

    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showInfo"{
            if let vc = segue.destination as? itemInfoTest {
                let menu = sender as? Item
               // print(menu ?? "nill")
                vc.itemToDisplay = menu

            }
        }
    }
    
}

  extension ItemsVC: UICollectionViewDataSource, UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let itemCell =  collectionView.dequeueReusableCell(withReuseIdentifier: "menuCell", for: indexPath) as? MenuCollectionViewCell{
            itemCell.menu = itemArray[indexPath.row]
            return itemCell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let menu = itemArray[indexPath.row]
        self.performSegue(withIdentifier: "showInfo", sender: menu)
        
    }
    
//    private func saveNoteWith() {
//        let note = PFObject(className: "Items")
//        note["name"] = "rrrrrr"
//
//        note.saveInBackground { (success, error) in
//            success ? self.reloadDataFromParse() : print(error!.localizedDescription)
//        }
//    }

    
    
//     func reloadDataFromParse()  {
//        let query = PFQuery(className: "Items")
//
//        query.findObjectsInBackground { (optionalObjects, error) in
//            if let realObjects = optionalObjects {
//                self.objects = realObjects
//                print(realObjects)
//            }
//            else {
//                print(error!)
//            }
//
//
//
//        }
//    }


    
    
}
    

    
    


    

