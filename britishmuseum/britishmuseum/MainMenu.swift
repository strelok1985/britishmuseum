//
//  MainMenu.swift
//  britishmuseum
//
//  Created by Andrey Bakanov on 4/4/19.
//  Copyright © 2019 Andrey Bakanov. All rights reserved.
//

import UIKit
import QuartzCore
import SceneKit
import SafariServices

class MainMenu: UIViewController {
    var openTime = 17

    @IBOutlet weak var workTime: UILabel!
    
    @IBOutlet weak var timeToClose: UILabel!
    
    override func viewDidLoad() {
    super.viewDidLoad()
        getWorkTime()
    }
    
    func getWorkTime(){
        let dayOf: [String] = ["01.01", "24.12", "25.12", "26.12"]
        let date = Date()
        let calendar = Calendar.current
        let weekday = calendar.component(.weekday, from: date)

        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM"
        let dateNumber = formatter.string(from: date)
        
        if weekday == 6
        {
            openTime = 20
        }
        
        var dateComponents = DateComponents()
        dateComponents.hour = openTime
        dateComponents.minute = 30
        dateComponents.second = 00
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        let dateFromDC = calendar.date(from: dateComponents)
    
        let timeInterval = (dateFromDC?.timeIntervalSinceNow)!
    
        let po : Int = Int(timeInterval)
        let s = po % 60
        let min =  (po - s) %  3600  / 60
        let hour = (po - ( po % 3600) ) / 3600
    
          print("\(timeInterval) ")
        
        
        
//        for i in 0..<dayOf.count {
//            if dayOf[i] == dateNumber{
//                print("\(dayOf[i])")
//                workTime.text = ("Музей Закрыт")
//                }
//        }
        if dayOf.contains("\(dateNumber)") || po < 0 {
            workTime.text = ("Музей Закрыт")
            timeToClose.text = ("--------")
        }
              else if weekday == 6
            {
                workTime.text = (" с 10.00 до 20.30 часов")
                timeToClose.text = (" \(hour) ч. \(min) мин")
            }
            else {workTime.text = (" с 10.00 до 17.30 часов")
            timeToClose.text = (" \(hour) ч. \(min) мин")
        }
        }
    
    @IBAction func buyApp(_ sender: Any) {
        showAlert1()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    func buyApp() {
        print("buyApp")
        }
    
    func showAlert1() -> Void {
        let alertConreoller = UIAlertController(title: "Благодарим",
                                                message: "За покупку аудиогида",
                                                preferredStyle: .alert)
        
        let cancelAction: ((UIAlertAction) -> Void)? = ({ (act) in
            self.buyApp()
        })
       
        alertConreoller.addAction(UIAlertAction(title: "Спасибо",
                                                style: .cancel,
                                                handler: cancelAction))
        
        present(alertConreoller, animated: true)
        
        
    }

    @IBAction func showWeb(_ sender: UIButton) {
        if let url = URL(string: "https://www.britishmuseum.org/") {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            
            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }
    
   
    
}
