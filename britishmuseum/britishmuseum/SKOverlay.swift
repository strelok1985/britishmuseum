//
//  SKOverlay.swift
//  britishmuseum
//
//  Created by Strelok on 5/21/19.
//  Copyright © 2019 Andrey Bakanov. All rights reserved.
//

import UIKit
import SpriteKit

class SKOverlay: SKScene {
    
    weak var mainView: Map?
    var showLayer = SKSpriteNode()
    var showFirstLevel = SKSpriteNode()
    var showSecondLevel = SKSpriteNode()
    
    convenience init(main: Map, size: CGSize){
        self.init(sceneSize: size)
        mainView = main
    }
    
    convenience init(sceneSize: CGSize){
        self.init(size: sceneSize)
        
        let layerTexture = SKTexture(image: UIImage(named: "layers")!)
        showLayer = SKSpriteNode(texture: layerTexture)
        showLayer.size = CGSize(width: 20, height: 20)
        showLayer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        showLayer.position = CGPoint(x: 300, y: 500)
        showLayer.name = "levels"
        
        self.addChild(showLayer)
        
        let layerTexture1 = SKTexture(image: UIImage(named: "1_flour")!)
        showFirstLevel = SKSpriteNode(texture: layerTexture1)
        showFirstLevel.size = CGSize(width: 20, height: 20)
        showFirstLevel.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        showFirstLevel.position = CGPoint(x: 260, y: 500)
        showFirstLevel.name = "1_flour"
        
        self.addChild(showFirstLevel)
        
        let layerTexture2 = SKTexture(image: UIImage(named: "2_flour")!)
        showSecondLevel = SKSpriteNode(texture: layerTexture2)
        showSecondLevel.size = CGSize(width: 20, height: 20)
        showSecondLevel.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        showSecondLevel.position = CGPoint(x: 220, y: 500)
        showSecondLevel.name = "2_flour"
        
        self.addChild(showSecondLevel)
    }
    
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let main =  mainView{
            main.touchesFunction(touches, with: event)
        }
    }
    
}

